﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MVC_DI.Models;
using MVC_DI.Resources;

namespace MVC_DI.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IStudentResource _studentResource;

        public HomeController(ILogger<HomeController> logger, IStudentResource studentResource)
        {
            _logger = logger;
            _studentResource = studentResource;
        }

        public IActionResult Index()
        {
            string Message = $"About page visited at {DateTime.UtcNow.ToLongTimeString()}";
            _logger.LogError(Message);
            int temp = _studentResource.GenrateRandomNumber();
            _logger.LogError("Random number from dependency injection is = " + temp);
            return View();
      
        }

        public IActionResult Privacy()
        {
            int temp = _studentResource.GenrateRandomNumber();
            _logger.LogError("Random number from dependency injection is = " + temp);
            return View();

        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
